---
title: README
author: Joseph Tran
date: 2024-03-11
---

## Quarto Shiny SK8

Présentation de l'atelier au format Revealjs: [https://baric.pages.mia.inra.fr/quarto-shiny-sk8](https://baric.pages.mia.inra.fr/quarto-shiny-sk8)
